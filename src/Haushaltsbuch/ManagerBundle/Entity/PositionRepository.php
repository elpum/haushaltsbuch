<?php

namespace Haushaltsbuch\ManagerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PositionRepository extends EntityRepository
{
    public function getSumFromAllPositions()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('SUM(p.value)')
            ->from('HaushaltsbuchManagerBundle:Position','p')
            ->getQuery();
        $result = $query->getSingleResult();

        return is_null($result) ? 0 : $result[1];
    }
}