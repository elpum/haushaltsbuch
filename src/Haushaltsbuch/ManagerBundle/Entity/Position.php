<?php

namespace Haushaltsbuch\ManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Haushaltsbuch\ManagerBundle\Entity\PositionRepository;

/**
 * Class Position
 * @package Haushaltsbuch\ManagerBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="position")
 * @ORM\Entity(repositoryClass="Haushaltsbuch\ManagerBundle\Entity\PositionRepository")
 */
class Position {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    protected $value;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Assert\NotBlank()
     */
    protected $date;

    /**
     * Position constructor.
     * @param $date
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }


    /**
     * @return Integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param integer $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}