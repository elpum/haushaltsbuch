<?php

namespace Haushaltsbuch\ManagerBundle\Controller;

use Haushaltsbuch\ManagerBundle\Entity\Position;
use Haushaltsbuch\ManagerBundle\Form\PositionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HaushaltController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('HaushaltsbuchManagerBundle:Position');
        $allPositions = $repository->findAll();
        $allPositionSum = $repository->getSumFromAllPositions();

        return $this->render('HaushaltsbuchManagerBundle:Haushalt:index.html.twig', array(
            'positions' => $allPositions,
            'sumOfAllPositions' => $allPositionSum,
        ));
    }

    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $position = $em->getRepository('HaushaltsbuchManagerBundle:Position')->find($id);

        return $this->render('HaushaltsbuchManagerBundle:Haushalt:detail.html.twig', array(
            'position' => $position
        ));
    }

    public function newAction()
    {
        $position = new Position();

        $form = $this->createForm(new PositionType(), $position, array(
            'action' => $this->generateUrl('haushaltsbuch_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Speichern'));

        return $this->render('HaushaltsbuchManagerBundle:Haushalt:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        $position = new Position();

        $form = $this->createForm(new PositionType(), $position, array(
            'action' => $this->generateUrl('haushaltsbuch_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Speichern'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($position);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg', 'Position wurde gespeichert');

            return $this->redirect($this->generateUrl('haushaltsbuch_details', array('id' => $position->getId())));
        }

        $this->get('session')->getFlashBag()->add('msg', 'Bitte alle Felder korrekt ausfüllen');

        return $this->render('HaushaltsbuchManagerBundle:Haushalt:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $position = $em->getRepository('HaushaltsbuchManagerBundle:Position')->find($id);

        $form = $this->createForm(new PositionType(), $position, array(
            'action' => $this->generateUrl('haushaltsbuch_update', array('id' => $position->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Bearbeiten'));

        return $this->render('HaushaltsbuchManagerBundle:Haushalt:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $position = $em->getRepository('HaushaltsbuchManagerBundle:Position')->find($id);

        $form = $this->createForm(new PositionType(), $position, array(
            'action' => $this->generateUrl('haushaltsbuch_update', array('id' => $position->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Bearbeiten'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg', 'Position wurde bearbeitet');

            return $this->redirect($this->generateUrl('haushaltsbuch_details', array('id' => $position->getId())));
        }
        return $this->render('HaushaltsbuchManagerBundle:Haushalt:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function removeAction($id)
    {
        $deleteForm = $this->createFormBuilder()
            ->setAction($this->generateUrl('haushaltsbuch_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Wirklich löschen'))
            ->getForm();

        return $this->render('HaushaltsbuchManagerBundle:Haushalt:remove.html.twig', array(
            'form' => $deleteForm->createView()
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $deleteForm = $this->createFormBuilder()
            ->setAction($this->generateUrl('haushaltsbuch_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Wirklich löschen'))
            ->getForm();

        $deleteForm->handleRequest($request);

        if($deleteForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $position = $em->getRepository('HaushaltsbuchManagerBundle:Position')->find($id);

            $em->remove($position);

            $em->flush();

            $this->get('session')->getFlashBag()->add('msg', 'Position wurde gelöscht');

            return $this->redirect($this->generateUrl('haushaltsbuch'));
        }
        $this->get('session')->getFlashBag()->add('msg', 'Position konnte nicht gelöscht werden');

        return $this->redirect($this->generateUrl('haushaltsbuch'));
    }
}
